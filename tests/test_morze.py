import unittest
from task import morze

class TaskTest(unittest.TestCase):
    def test_task_1(self):
        res_1 = morze.code_morze("Ambitious")
        self.assertEqual(res_1, ".- -- -... .. - .. --- ..- ...")


    def test_task_2(self):    
        res_2 = morze.code_morze("Never give in")
        self.assertEqual(res_2, "-. . ...- . .-. --. .. ...- . .. -.")

    def test_task_3(self):    
        res_3 = morze.code_morze("Data Science-2022")
        self.assertEqual(res_3, "-.. .- - .- ... -.-. .. . -. -.-. . -....- ..--- ----- ..--- ..---")
